var getCookie = function (name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
};

var cookieCheck = function() {
  var _pckflag = getCookie('_pckflag');
  if (!_pckflag) {
    document.cookie = "_pckflag=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
    $('#cookie-info').addClass('show');
  }
};

$('#cookie-info').click(function() {
  document.cookie = "_pckflag=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
  $('#cookie-info').removeClass('show');
});

$(document).ready(function() {
  cookieCheck();
});
