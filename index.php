<?php include "header.php"; ?>

<div id="home-slider">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide" style="background-image: url('assets/img/slider/1.jpg')"></div>
      <div class="swiper-slide" style="background-image: url('assets/img/slider/2.jpg')"></div>
      <div class="swiper-slide" style="background-image: url('assets/img/slider/3.jpg')"></div>
      <div class="swiper-slide" style="background-image: url('assets/img/slider/4.jpg')"></div>
    </div>
  </div>
  <div class="slider-nav-wrap">
    <div class="container _mid">
      <div class="row">
        <div class="col-sm-3">
          <div class="slider-nav">
            <div class="_title" data-slideindex="0">
              <div class="_top">
                <i class="far fa-compass"></i>
                <div class="_name ellipsis-on-overflow">Explore Nepal with Pinnacle</div>
              </div>
              <div class="slide-progress">
                <div class="_bar"></div>
              </div>
            </div>
            <div class="_body">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt lobortis nisl 
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="slider-nav">
            <div class="_title" data-slideindex="1">
              <div class="_top">
                  <i class="fas fa-plane"></i>
                <div class="_name ellipsis-on-overflow">Exclusive Deals</div>
              </div>
              <div class="slide-progress">
                <div class="_bar"></div>
              </div>
            </div>
            <div class="_body">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt lobortis nisl 
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="slider-nav">
            <div class="_title" data-slideindex="2">
              <div class="_top">
                <i class="fas fa-coins"></i>
                <div class="_name ellipsis-on-overflow">Best Rates</div>
              </div>
              <div class="slide-progress">
                <div class="_bar"></div>
              </div>
            </div>
            <div class="_body">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt lobortis nisl 
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="slider-nav">
            <div class="_title" data-slideindex="3">
              <div class="_top">
                <i class="fas fa-crown"></i>
                <div class="_name ellipsis-on-overflow">Complete Care</div>
              </div>
              <div class="slide-progress">
                <div class="_bar"></div>
              </div>
            </div>
            <div class="_body">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diamLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt lobortis nisl 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section id="discover">
  <div class="container _mid">
    <div class="row">
      <div class="col">
        <div class="discover-bar">
          <div class="_title">
            <span>Discover </span> the best of Pinnacle
          </div>
          <a href="#">Show all journeys <span class="_icon _arrow_right"></span></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div id="package-slider">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="listing-item">
                <div class="_image">
                  <a href="#">
                    <img src="assets/img/listing/bhaktapur.jpg" alt="">
                    <div class="_location">Pokhara</div>
                  </a>
                </div>
                <div class="_details">
                  <div class="_duration">6 days trip</div>
                  <div class="_name">
                      Package Description & will go here
                      title goes heree goes destination                
                  </div>
                  <div class="_info">
                    <div class="_price">
                      NPR. 13,000 per person
                    </div>
                    <div class="_rating">
                      <span>3.5</span>
                      <div class="rating-stars" data-stars="3"></div>
                    </div>
                  </div>
                  <div class="_link">
                    <a href="#">Get Package</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="listing-item">
                <div class="_image">
                  <a href="#">
                    <img src="assets/img/listing/farcry.jpg" alt="">
                    <div class="_location">Upper Mustang</div>
                  </a>
                </div>
                <div class="_details">
                  <div class="_duration">1-2 weeks trip</div>
                  <div class="_name">
                      Package Description & will go here
                      title goes heree goes destination                
                  </div>
                  <div class="_info">
                    <div class="_price">
                      NPR. 13,000 per person
                    </div>
                    <div class="_rating">
                      <span>3.5</span>
                      <div class="rating-stars" data-stars="3"></div>
                    </div>
                  </div>
                  <div class="_link">
                    <a href="#">Get Package</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="listing-item">
                <div class="_image">
                  <a href="#">
                    <img src="assets/img/listing/mountain.jpg" alt="">
                    <div class="_location">Bouddhanath</div>
                  </a>
                </div>
                <div class="_details">
                  <div class="_duration">12 days trip</div>
                  <div class="_name">
                      Package Description & will go here
                      title goes heree goes destination                
                  </div>
                  <div class="_info">
                    <div class="_price">
                      NPR. 13,000 per person
                    </div>
                    <div class="_rating">
                      <span>4</span>
                      <div class="rating-stars" data-stars="4"></div>
                    </div>
                  </div>
                  <div class="_link">
                    <a href="#">Get Package</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="listing-item">
                <div class="_image">
                  <a href="#">
                    <img src="assets/img/listing/bhutan.jpg" alt="">
                    <div class="_location">Lower Mustang</div>
                  </a>
                </div>
                <div class="_details">
                  <div class="_duration">6 days trip</div>
                  <div class="_name">
                      Package Description & will go here
                      title goes heree goes destination                
                  </div>
                  <div class="_info">
                    <div class="_price">
                      NPR. 13,000 per person
                    </div>
                    <div class="_rating">
                      <span>5</span>
                      <div class="rating-stars" data-stars="5"></div>
                    </div>
                  </div>
                  <div class="_link">
                    <a href="#">Get Package</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="listing-item">
                <div class="_image">
                  <a href="#">
                    <img src="assets/img/listing/swoyambhu.jpg" alt="">
                    <div class="_location">Lower Mustang</div>
                  </a>
                </div>
                <div class="_details">
                  <div class="_duration">6 days trip</div>
                  <div class="_name">
                      Package Description & will go here
                      title goes heree goes destination                
                  </div>
                  <div class="_info">
                    <div class="_price">
                      NPR. 13,000 per person
                    </div>
                    <div class="_rating">
                      <span>1</span>
                      <div class="rating-stars" data-stars="1"></div>
                    </div>
                  </div>
                  <div class="_link">
                    <a href="#">Get Package</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="_nav">
            <div class="swiper-pagination"></div>

            <div class="package-slider-prev">
              <img src="assets/icons/small-arrow-left.png" alt="">
            </div>
            <div class="package-slider-next">
              <img src="assets/icons/small-arrow-right.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="book-guide">
  <div class="head">
    <div class="container _mid">
      <div class="row">
        <div class="col-7">
          <div class="_subtitle">
            Book <span>a suitable guide</span>
          </div>
          <div class="_title">
            Hire a Tour Guide
          </div>
          <div class="_desc">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam sunt odit id culpa eum ipsa sint ipsam tempora perspiciatis aliquam fuga, pariatur facere voluptatibus consequuntur! Fugit ipsam officiis facilis quam?
          </div>
        </div>
        <div class="col-5 d-flex justify-content-end align-items-start">
          <button id="showBookForm" class="_primary _orange">Hire Now</button>
          <button id="hideBookForm"><i class="fas fa-times"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div id="book-form">
    <div class="_main">
      <div class="container _mid">
        <div class="row">
          <div class="col d-flex justify-content-center">
            <div class="form _input_centered">
              <div class="control-row">
                <input type="text" placeholder="Full Name">
              </div>
              <div class="control-row">
                <input type="email" placeholder="Email Address">
              </div>
              <div class="control-row">
                <input type="tel" placeholder="Phone Number">
              </div>
              <div class="control-row">
                <input type="data" placeholder="Visit Date">
              </div>
              <div class="control-row">
                <input type="number" placeholder="Number of Pax">
              </div>
              <div class="control-row">
                <input type="text" placeholder="Guide Preferred Language">
              </div>
              <div class="contol-row">
                <button class="_primary _orange _no_border _full">Hire Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="places">
  <div id="map-wrapper">
    <img src="assets/maps/nepalMap.svg" alt="">
  </div>
  <div class="markers">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="box">
            <div class="row">
              <div class="col-sm-3">
                <i class="far fa-compass"></i>
                <div class="_title">
                  Places <br>
                  for you to <br>
                  experience
                </div>
              </div>
              <div class="col-sm-3"></div>
              <div class="col-sm-3"></div>
              <div class="col-sm-3"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="instagram">
  <div class="_head">
    <i class="fab fa-instagram"></i>
    <div>#pinnacleexpedition</div>
    <span>View top  experiences through instagram</span>
  </div>
  <div class="tag-list">
    <div class="container _mid">
      <div class="row">
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/3.jpg" alt="">
              <div class="tag">
                <div class="_name">#MANANG</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/bhaktapur.jpg" alt="">
              <div class="tag">
                <div class="_name">#MUSTANG</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/bhutan.jpg" alt="">
              <div class="tag">
                <div class="_name">#MANANG</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/farcry.jpg" alt="">
              <div class="tag">
                <div class="_name">#GOKYO</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/mountain.jpg" alt="">
              <div class="tag">
                <div class="_name">#RARA</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="#">
            <div class="tag-box">
              <img src="assets/img/thumbnails/swoyambhu.jpg" alt="">
              <div class="tag">
                <div class="_name">#PATAN</div>
                <div>Holiday Destination</div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="best-rates">
  <div class="container _mid">
    <div class="row">
      <div class="col d-flex justify-content-between">
        <div class="_info">
          <div class="_subtitle">Book <span>a suitable package</span></div>
          <div class="_title">Get the <span>best</span> rates than any other place</div>
          <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.
          </p>
        </div>
        <a href="#" id="showBookForm" class="_link _primary _orange">View Packages</a href="#">
      </div>
    </div>
  </div>
</section>
<section id="brief">
  <div class="container _small">
    <div class="row">
      <div class="col-sm-3">
        <div class="_title">Know your packages</div>
        <div class="_body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Culpa earum commodi eius optio. Fugiat ipsum facilis magnam cupiditate nihil accusantium alias dicta doloribus, voluptatem voluptas fuga quasi aliquid ipsa facere?</div>
      </div>
      <div class="col-sm-3 offset-sm-1">
        <div class="_title">Travel with Pinnacle</div>
        <div class="_body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Culpa earum commodi eius optio. Fugiat ipsum facilis magnam cupiditate nihil accusantium alias dicta doloribus, voluptatem voluptas fuga quasi aliquid ipsa facere?</div>
      </div>
      <div class="col-sm-3 offset-sm-1">
        <div class="_title">Why Pinnacle works</div>
        <div class="_body">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Culpa earum commodi eius optio. Fugiat ipsum facilis magnam cupiditate nihil accusantium alias dicta doloribus, voluptatem voluptas fuga quasi aliquid ipsa facere?</div>
      </div>
    </div>
  </div>
</section>

<script>
  var SLIDER_DURATION = 5000;

  $(document).ready(function() {

    // Main Slider
    var mySwiper = new Swiper('.swiper-container', {
      autoplay: {
        delay: SLIDER_DURATION,
        disableOnInteraction: false
      }
    });

    var slideProgress = function() {
      var slideIndex = mySwiper.activeIndex;
      $('*[data-slideindex="'+slideIndex+'"]')
        .children('.slide-progress')
        .children('._bar')
        .animate({
          width: '100%'
        }, SLIDER_DURATION, function() {
          $('._bar').width(0);
        });
      }
      
    mySwiper.on('slideChange', function () {
      $('._bar').stop().width(0);
      slideProgress();
    });

    mySwiper.on('init', slideProgress());

    $('.slider-nav ._title').click(function() {
      var index = $(this).data('slideindex');
      mySwiper.slideTo(index);
    });

    // package Slider

    var arrangeArrows = function() {
      var containerWidth = $('#package-slider').width();
      var paginationWidth = $('#package-slider .swiper-pagination').width();
      $('.package-slider-prev').css('left', (containerWidth - paginationWidth) / 2 - 15);
      $('.package-slider-next').css('left', (containerWidth - paginationWidth) / 2 + 7 + paginationWidth);
    };

    var packageSlider = new Swiper('#package-slider', {
      slidesPerView: 4,
      spaceBetween: 100,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },
      navigation: {
        nextEl: '.package-slider-next',
        prevEl: '.package-slider-prev'
      }
    });

    packageSlider.on('paginationRender', arrangeArrows());

    // Book Form

    $('#showBookForm').click(function() {
      $('#book-guide').addClass('show');
    });

    $('#hideBookForm').click(function() {
      $('#book-guide').removeClass('show');
    });

  });
</script>

<?php include "footer.php"; ?>