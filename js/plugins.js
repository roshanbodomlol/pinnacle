// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.

// Rating Stars

$('.rating-stars').each(function(i, e) {
  var rating = $(e).data('stars');
  var template = '';
  for (let i = 0; i < rating; i++) {
    template += '<span class="_star"></span>';
  }
  for (let i = 0; i < (5 - rating); i++) {
    template += '<span class="_star _empty"></span>';
  }
  $(e).append(template).addClass('ready');
});

$(document).ready(function() {
  var bannerSlider = new Swiper('#banner', {
    autoplay: true,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
  });
});
