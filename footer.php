  <div id="footer-wrapper">
    <div class="container _large">
      <div class="row">
        <div class="col d-flex justify-content-between">
          <div class="_left">
            <div class="company-icon">
              <img src="assets/icons/pinnacle-icon.png" alt="">
            </div>
            <div class="_brief">
              Pinnacle is a leading player in online flight bookings in India, MakeMyTrip offers great offers, some of the lowest airfares, exclusive.
            </div>
            <ul id="social-icons" class="list">
              <li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a></li>
              <li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-youtube"></i></a></li>
            </ul>
            <div class="_copy">&copy; 2019 Pinnacle Expedition</div>
          </div>
          <div class="_right">
            <div class="row">
              <div class="col">
                <div class="footer-list">
                  <div class="_title">What we offer</div>
                  <ul>
                    <li><a href="#">Travel Packages</a></li>
                    <li><a href="#">Consulatants</a></li>
                    <li><a href="#">Find a guide</a></li>
                  </ul>
                </div>
              </div>
              <div class="col">
                <div class="footer-list">
                  <div class="_title">Pinnacle Expedition</div>
                  <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Experience</a></li>
                  </ul>
                </div>
              </div>
              <div class="col">
                <div class="footer-list">
                  <div class="_title">Information</div>
                  <ul>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">Visa Information</a></li>
                  </ul>
                </div>
              </div>
              <div class="col">
                <div class="footer-list">
                  <div class="_title">Our Packages</div>
                  <ul>
                    <li><a href="#">Package 1</a></li>
                    <li><a href="#">Package 2</a></li>
                    <li><a href="#">Package 3</a></li>
                    <li><a href="#">Package 4</a></li>
                  </ul>
                </div>
              </div>
              <div class="col">
                <div class="footer-list">
                  <div class="_title">Locations</div>
                  <ul>
                    <li><a href="#">Manang</a></li>
                    <li><a href="#">Rara Lake</a></li>
                    <li><a href="#">Mount Everest</a></li>
                    <li><a href="#">Kathmandu</a></li>
                  </ul>
                </div>
              </div>
              <div class="col">
                <div class="footer-list">
                  <div class="_title">Get Involved</div>
                  <ul>
                    <li><a href="#">Share your experience</a></li>
                    <li><a href="#">Travel with us</a></li>
                    <li><a href="#">Group Tips</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </div>
</div>

  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>

</html>