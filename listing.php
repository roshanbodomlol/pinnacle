<?php include "header.php"; ?>

<div id="banner">
  <div class="swiper-wrapper">
    <div class="swiper-slide" style="background-image: url(assets/img/slider/1.jpg)">
      <div class="container _mid">
        <div class="row">
          <div class="col d-flex align-items-end">
            <div class="_caption">
              <div class="_title">Explore Nepal</div>
              <div class="_desc">Lorem ipsum dolor sit amet, consectetuer diamLorem ipsum dolor sit amet, consectetuer
                  Lorem ipsum dolor um dolor sit amsit.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide" style="background-image: url(assets/img/slider/2.jpg)">
      <div class="container _mid">
        <div class="row">
          <div class="col d-flex align-items-end">
            <div class="_caption">
              <div class="_title">Explore Nepal</div>
              <div class="_desc">Lorem ipsum dolor sit amet, consectetuer diamLorem ipsum dolor sit amet, consectetuer
                  Lorem ipsum dolor um dolor sit amsit.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide" style="background-image: url(assets/img/slider/3.jpg)">
      <div class="container _mid">
        <div class="row">
          <div class="col d-flex align-items-end">
            <div class="_caption">
              <div class="_title">Explore Nepal</div>
              <div class="_desc">Lorem ipsum dolor sit amet, consectetuer diamLorem ipsum dolor sit amet, consectetuer
                  Lorem ipsum dolor um dolor sit amsit.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section id="info">
  <div class="container _mid">
    <div class="row">
      <div class="col-sm-4">
        <div class="_title">
          <span class="overline">Find</span> the perfect Package for you
        </div>
      </div>
      <div class="col-sm-7 offset-sm-1">
        <div class="_desc">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti dolorum impedit officia ipsum.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum iure autem nesciunt quibusdam odio libero dolor veritatis. Quo iusto, ipsum dolore, possimus minus placeat voluptates, maxime debitis odio ea totam?</p>    
        </div>
      </div>
    </div>
  </div>
</section>

<?php include "footer.php"; ?>