<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Pinnacle Expeditions</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,900|Caveat+Brush&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="dist/css/global.min.css">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="css/swiper.min.css">

  <meta name="theme-color" content="#fafafa">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
  <div id="wrapper">
    <div id="cookie-info" role="button">
      <div class="_inner">
        <i class="fas fa-info-circle"></i>
        <span>Let out site be more useful to you everytime you visit by enabling your
          cookies so we can remember details for a smoother browsing experience.
          <i class="fas fa-arrow-alt-circle-right"></i>
          <b>I AGREE</b>
        </span>
      </div>
    </div>
    <div id="header-wrapper">
      <div class="_top">
        <div class="container _large">
          <div class="row">
            <div class="col d-flex justify-content-end">
              <ul class="list">
                <li><a href="#">Find a Guide</a></li>
                <li><a href="#">Travel Packages</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="_bottom">
        <div class="container _large">
          <div class="row">
            <div class="col d-flex align-items-center">
              <div id="logo">
                <a href="#"><img src="assets/img/logo.jpg" alt="Pinnacle Expeditions"></a>
              </div>
              <ul id="main-menu" class="list d-flex justify-content-center flex-grow-1">
                <li><a href="#">About</a></li>
                <li><a href="#">Travel Packages</a></li>
                <li><a href="#">Our Experience</a></li>
                <li><a href="#">Talk to us</a></li>
              </ul>
              <div class="_align"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="content-wrapper">